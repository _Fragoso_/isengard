# Isengard
**Isengard** is a Django application that searchs news from three different web sources using web scraping and save them to a `PostgreSQL`
database. Also the application downloads a `PDF` file from the web or from the database.

* [Dependencies](#dependencies)
* [Configuration](#configuration)

## Dependencies
![Python +3.7](https://img.shields.io/badge/python-+3.7-blue.svg)
![Docker](https://img.shields.io/badge/docker-19.03.8-blue.svg)
![PostgreSQL](https://img.shields.io/badge/postgres-*-blue.svg)

*Notes: You need to install previously **pipenv**, **docker** and **git**.*

## Configuration

First you have clone this repository for the following command.
```
git clone git@bitbucket.org:_Fragoso_/isengard.git
```

Then inside the project directory, execute the following command.
```
cp .env.example .env
```
This command create the `.env` file, that will hold the environment variables containing the private access credentials of the database server.


The variables contained inside the `.env` file are listed below.

### **Environment variables**

| Variable                                          | Value                |
|---------------------------------------------------|----------------------|
| [POSTGRESQL_DB_PASSWORD](#postgresql_db_password) | password             |
| [POSTGRESQL_DB_USER](#postgresql_db_user)         | postgres             |
| [POSTGRESQL_DB_NAME](#postgresql_db_name)         | isengard_db          |
| [EL_PAIS_URL](#el_pais_url)                       | <el_pais_url_website>|
| [REFORMA_URL](#reforma_url)                       | <reforma_url_website> |
| [EL_UNIVERSAL_URL](#el_universal_url)             | <el_universal_url_website> 
| [POSTGRESQL_DB_HOST](#postgresql_db_host)         | 127.0.0.1
|
| [POSTGRESQL_DB_PORT](#postgresql_db_port)         | 50740 |

Install dependencies with the comand below.
```
pipenv install --three --dev --pre
```

Create the `PostgreSQL` database server with the commands listed below.
```
docker pull postgres

docker network create \ 
--driver=bridge isengard_network

docker run -d --name isengard_postgres \ 
-v pgdata:/var/lib/postgresql/data \
--network=isengard_network \
-e POSTGRES_PASSWORD=password -p 50740:5432 postgres
```

Create the `isengard_db` database.
```
docker exec -it isengard_postgres bash

(Inside the container shell execute)
psql -U postgres

CREATE DATABASE isengard_db;
GRANT ALL PRIVILEGES ON DATABASE isengard_db TO postgres;
```

Go back to the root project directory, and execute the following commands to activate the environment and run the application.
    
```
pipenv shell --fancy
python3 manage.py migrate
python3 manage.py runserver
```

Now you have the application running inside your localhost.


If you want to run the Django application inside a Docker container, you want to execute the following commands inside
the project root directory, after you have modified the
following `.env` variables with the following values.

`POSTGRESQL_DB_HOST=isengard_postgres`
`POSTGRESQL_DB_PORT=5432`

```
docker build -f Dockerfile  -t isengard:1.0 .
docker run --restart=always \
--name=isengard_project \
--link=isengard_postgres:postgres \ --network=isengard_network \
--env-file .env -p 8095:7070 -d isengard:1.0
```

**FINAL NOTE**: The results obtained for a `POST` method and for a download process are contained inside the `results` directory.

