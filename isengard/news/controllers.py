import os
import re
import requests
import html5lib
from bs4 import BeautifulSoup
from dataclasses import dataclass
from typing import List


@dataclass
class ElPaisScrapper:
    """
    Class for obtain a set of news from `EL PAÍS` website,
    using web scrapping.

    Parameters
    ----------
    url : str
        The URL of the service.
    
    Returns
    -------
    `ElPaisScrapper` instance.
    """

    url: str

    def get_news_information(self) -> list:

        page_response = requests.get(self.url)
        page_content = page_response.content

        page_soup = BeautifulSoup(page_content, "html5lib")

        regex = re.compile("headline.*")

        news_obtained = page_soup.find_all("h2", {"class": regex})

        news_information = [
            {
                "title": new.get_text(),
                "content": self._obtain_new_content(
                    "https://elpais.com" + new.find("a")["href"]
                ),
            }
            for new in news_obtained[:5]
        ]

        filtered_information = [
            information
            for information in news_information
            if len(information["content"]) != 0
        ]

        return filtered_information

    def _obtain_new_content(self, url: str) -> str:
        new_content = requests.get(url).content
        new_content_soup = BeautifulSoup(new_content, "html5lib")
        new_content_obtained = new_content_soup.find_all("p", {"class": ""})

        formatted_new_content = [
            element.get_text()
            for element in new_content_obtained
            if ("class" in element.attrs and element.a is None and element.b is None)
        ]

        if not formatted_new_content:
            new_content_obtained = new_content_soup.find_all("p")
            formatted_new_content = [
                element.get_text()
                for element in new_content_obtained
                if (
                    "class" not in element.attrs
                    and element.a is None
                    and element.b is None
                )
            ]

        final_text_content = "\n\n".join(formatted_new_content)
        return final_text_content


@dataclass
class ReformaScrapper:
    url: str

    def get_news_information(self) -> list:

        response = requests.get(self.url)
        page_content = response.content

        soup = BeautifulSoup(page_content, "html5lib")

        news_obtained = soup.find_all("a", class_="ligaonclick")
        filtered_news = [
            element for element in news_obtained[:5] if "appFC" not in element.get_text()
        ]

        last_filtered_news: List = []

        for element in filtered_news:
            try:
                int(element.get_text())
            except:
                if len(element.get_text()) not in [0, 1]:
                    last_filtered_news.append(element)

        news_information = [
            {
                "title": title.get_text(separator=" ").strip(),
                "content": self._obtain_new_content(
                    "https://www.reforma.com/aplicacioneslibre/preacceso/articulo/default.aspx?__rval=1&urlredirect=https://www.reforma.com/"
                    + title["href"]
                ),
            }
            for index, title in enumerate(last_filtered_news)
            if index % 2 == 0 and "gruporeforma" not in title["href"]
        ]

        filtered_information = [
            information
            for information in news_information
            if len(information["content"]) != 0
        ]

        return filtered_information

    def _obtain_new_content(self, url: str) -> str:
        news_content = requests.get(url).content
        news_soup = BeautifulSoup(news_content, "html5lib")

        news_obtained = news_soup.find_all("meta", {"name": "twitter:description"})

        news_description = [new["content"] for new in news_obtained]

        final_text_content = "\n\n".join(news_description)

        return final_text_content


@dataclass
class ElUniversalScrapper:
    url: str

    def get_news_information(self) -> list:

        response = requests.get(self.url)
        page_content = response.content

        soup = BeautifulSoup(page_content, "html5lib")

        news_obtained = soup.find_all("div", class_="views-field views-field-title")

        filtered_news = [new for new in news_obtained[:5] if new.h4 is None]

        last_filtered_titles = [filter_new.get_text() for filter_new in filtered_news]

        last_filtered_urls = [
            filter_data.find("a")["href"]
            if "eluniversal.com" in filter_data.find("a")["href"]
            else "https://www.eluniversal.com.mx" + filter_data.find("a")["href"]
            for filter_data in filtered_news
        ]

        news_information = [
            {"title": new_title, "content": self._obtain_new_content(new_content)}
            for new_title, new_content in zip(last_filtered_titles, last_filtered_urls)
        ]

        filtered_information = [
            information
            for information in news_information
            if len(information["content"]) != 0
        ]

        return filtered_information

    def _obtain_new_content(self, url: str) -> str:

        page_content = requests.get(url).content
        page_soup = BeautifulSoup(page_content, "html5lib")

        paragraphs = page_soup.find_all("p")

        filtered_paragraphs = [
            element.get_text()
            for element in paragraphs
            if element.br is None and element.a is None
        ]

        final_text_content = "\n\n".join(filtered_paragraphs)

        return final_text_content
