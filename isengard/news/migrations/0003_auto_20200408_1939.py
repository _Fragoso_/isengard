# Generated by Django 3.0.3 on 2020-04-08 19:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("news", "0002_auto_20200408_1857"),
    ]

    operations = [
        migrations.RenameField(model_name="new", old_name="input_id", new_name="input",),
    ]
