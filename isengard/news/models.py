from django.db import models


class Input(models.Model):
    keywords = models.CharField(max_length=255)

    class Meta:
        db_table = "inputs"


class New(models.Model):
    ranking = models.FloatField()
    content = models.TextField()
    reference = models.URLField()
    input = models.ForeignKey(Input, on_delete=models.CASCADE, default=None)

    class Meta:
        db_table = "news"
