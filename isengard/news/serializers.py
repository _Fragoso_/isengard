from rest_framework import serializers
from .models import New


class NewSerializer(serializers.ModelSerializer):
    class Meta:
        model = New
        fields = ["ranking", "content", "reference"]

    def create(self, validated_data):
        return New.objects.create(
            input=self.context.get("input_created"), **validated_data
        )


class KeywordsSerializer(serializers.Serializer):
    keywords = serializers.ListField(
        child=serializers.CharField(), min_length=1, allow_empty=False, required=True
    )
