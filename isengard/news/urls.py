from rest_framework.routers import SimpleRouter
from .views import NewListViewSet

router = SimpleRouter()

router.register(r"news", NewListViewSet)

urlpatterns = router.urls
