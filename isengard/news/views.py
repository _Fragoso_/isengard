import os

from django.shortcuts import render
from rest_framework import status
from rest_framework.mixins import CreateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .controllers import ElPaisScrapper, ReformaScrapper, ElUniversalScrapper
from .models import New, Input
from .serializers import KeywordsSerializer, NewSerializer
from utils.text_analytics import TextAnalyzer
from utils.handling_data import sort_dict_list
from reportlab.pdfgen import canvas
from django.http import HttpResponse
from datetime import datetime
from reportlab.lib.pagesizes import A4
from textwrap import wrap


class NewListViewSet(GenericViewSet, CreateModelMixin):
    serializer_class = KeywordsSerializer
    queryset = New.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        keywords = serializer.data["keywords"]

        if self._search_input(keywords):
            keywords_formatted = " ".join(keywords)
            input_obtained = Input.objects.get(keywords=keywords_formatted)

            news_obtained = New.objects.filter(input=input_obtained.id)

            news_serializer = NewSerializer(news_obtained, many=True)

            if request.headers.get("Expect") == "application/pdf":
                response = HttpResponse(content_type="application/pdf")
                response["Content-Disposition"] = (
                    "attachment;"
                    + " "
                    + "filename="
                    + "_".join(str((datetime.now()).date()).split("-"))
                    + ".pdf"
                )

                w, h = A4
                p = canvas.Canvas(response, pagesize=A4)

                pdf_content: str = ""
                for element in news_serializer.data:
                    pdf_content += element["content"]

                text = p.beginText(50, h - 50)
                text.setFont("Times-Roman", 6)

                for element in pdf_content.split("\n"):
                    text.textLines("\n".join(wrap(element, 80)))

                p.drawText(text)
                p.showPage()
                p.save()

                return response

            return Response(
                data={"news": news_serializer.data}, status=status.HTTP_302_FOUND
            )

        el_pais_scrapper = ElPaisScrapper(os.environ.get("EL_PAIS_URL"))
        el_pais_news = el_pais_scrapper.get_news_information()
        el_pais_analyzer = TextAnalyzer(os.environ.get("EL_PAIS_URL"))
        el_pais_ranked_news = el_pais_analyzer.get_ranked_news(el_pais_news, keywords)

        reforma_scrapper = ReformaScrapper(os.environ.get("REFORMA_URL"))
        reforma_news = reforma_scrapper.get_news_information()
        reforma_analyzer = TextAnalyzer(os.environ.get("REFORMA_URL"))
        reforma_ranked_news = reforma_analyzer.get_ranked_news(reforma_news, keywords)

        el_universal_scrapper = ElUniversalScrapper(os.environ.get("EL_UNIVERSAL_URL"))
        el_universal_news = el_universal_scrapper.get_news_information()
        el_universal_analyzer = TextAnalyzer(os.environ.get("EL_UNIVERSAL_URL"))
        el_universal_ranked_news = el_universal_analyzer.get_ranked_news(
            el_universal_news, keywords
        )

        all_ranked_news = (
            el_pais_ranked_news + reforma_ranked_news + el_universal_ranked_news
        )

        first_three_ranked_news = sort_dict_list(all_ranked_news, key="ranking")[:3]

        if request.headers.get("Expect") == "application/pdf":
            response = HttpResponse(mimetype="application/pdf")
            response["Content-Disposition"] = (
                "attachment;"
                + " "
                + "filename="
                + "_".join(str((datetime.now()).date()).split("-"))
                + ".pdf"
            )

            p = canvas.Canvas(response)

            pdf_content: str = ""
            for element in first_three_ranked_news:
                pdf_content += element["content"]

            p.drawString(100, 100, pdf_content)
            p.showPage()
            p.save()

            return response

        self._save_news(first_three_ranked_news, keywords)

        return Response(
            data={"news": first_three_ranked_news}, status=status.HTTP_201_CREATED
        )

    def _save_news(self, news_list, keywords):
        keywords_formatted = " ".join(keywords)

        input_created = Input.objects.create(keywords=keywords_formatted)

        news_serializer = NewSerializer(
            data=news_list, context={"input_created": input_created}, many=True
        )

        news_serializer.is_valid(raise_exception=True)

        news_serializer.save()

    def _search_input(self, keywords) -> bool:
        keywords_formatted = " ".join(keywords)

        try:
            input_obtained = Input.objects.get(keywords=keywords_formatted)
        except:
            return False

        return True
