from dataclasses import dataclass
from typing import List
import statistics
from dataclasses import dataclass


@dataclass
class TextAnalyzer:
    url: str

    def get_ranked_news(self, news: list, keywords: list) -> object:
        ranked_news: List = []
        formatted_keywords = [word.lower() for word in keywords]

        for new_element in news:
            all_new_content_words = (
                (new_element["title"] + " " + new_element["content"])
                .lower()
                .replace("\n", " ")
                .split(" ")
            )

            keywords_ocurrencies = dict.fromkeys(formatted_keywords, 0)

            ranking = self._get_ranking(keywords_ocurrencies, all_new_content_words)

            ranked_news.append(
                {
                    "ranking": ranking,
                    "content": new_element["title"] + "\n" + new_element["content"],
                    "reference": self.url,
                }
            )

        return ranked_news

    def _get_ranking(
        self, keywords_ocurrencies: dict, all_new_content_words: list
    ) -> float:

        for word in keywords_ocurrencies.keys():
            if word in all_new_content_words:
                keywords_ocurrencies[word] += 1

        terms_frequencies = [
            ocurrencie / len(all_new_content_words)
            for ocurrencie in keywords_ocurrencies.values()
        ]

        return statistics.mean(terms_frequencies)
